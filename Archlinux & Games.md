####
Issue: Steam client and dota are slow to load and hangs when loading game, changing screen \
Solution: Disable Shader pre-caching \
Source: https://teddit.net/r/DotA2/comments/pbg4dd/dota2_slow_to_load_and_hangs_when_loading_game/hp1ejgz/ \
Date: 2022-05-25 \